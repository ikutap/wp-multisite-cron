<?php
/*
 * Plugin Name: WP Multisite Cron
 * Plugin URI: https://bitbucket.org/ikutap/wp-multisite-cron
 * Description: Plugin to handle WP-Cron jobs in multisite environments
 * Author: IkuTap
 * Version: 0.1
 * Author URI: http://ikutap.com/
 */

class WP_MultisiteCron {

    private static $instance = NULL;
    private static $lang = 'wp-multisite-cron';
    private static $lang_dir = '/lang/';

    /**
     *
     * Initialization logic
     */
    protected function __construct() {
        // Register plugin activation actions
        register_activation_hook(__FILE__, array(&$this, 'activate'));

        // Setup the translation
        load_plugin_textdomain(self::$lang, false, dirname(plugin_basename( __FILE__ ) ) . self::$lang_dir);

        // The multisite cron action
        add_action('wp_ajax_wp_multisite_cron_call', array(&$this,'cron_call'));
        add_action('wp_ajax_nopriv_wp_multisite_cron_call', array(&$this,'cron_call'));

        // Admin actions and hooks
        if (is_admin()) {
            $this->admin_hooks();
        }
    }

    /**
     *
     * Plugin activation (sets default parameters)
     */
    public function activate() {
        if ($this->get_option('concurrent_crons') === FALSE) {
            $this->update_option('concurrent_crons', 100);
        }
    }

    /**
     *
     * The admin hooks
     */
    public function admin_hooks() {
        // Setting menu
        add_action('admin_menu', array(&$this, 'admin_menu'));

        if (is_network_admin() && $this->is_ready()) {
            add_action('network_admin_menu', array(&$this, 'network_admin_menu'));
        }
    }

    /**
     *
     * Set up the admin menu(s)
     */
    public function admin_menu() {
        add_options_page("WordPress multisite cron, information page", "WP Multisite Cron", 'manage_options', 'wp_multisite_cron_settings', array(&$this, "admin_settings"));
    }

    /**
     *
     * Set up the network admin menu(s)
     */
    public function network_admin_menu() {
        add_submenu_page("settings.php","WordPress multisite cron, information page", "WP Multisite Cron", 'manage_options', 'wp_multisite_cron_settings', array(&$this, "admin_settings"));
    }


    /**
     *
     * The admin settings page
     */
    public function admin_settings() {
        ?>
        <div class="wrap">
            <div id="icon-options-general" class="icon32">
                <br />
            </div>
            <h2>WordPress multisite cron</h2>

            <?php if (isset($_POST['wp_multisite_cron']) && !empty($_POST['wp_multisite_cron'])) :

                if (isset($_POST['_wpnonce']) && wp_verify_nonce( $_POST['_wpnonce'], plugin_basename( __FILE__ ) )) :
                    $this->delete_option('concurrent_crons');

                    foreach($_POST['wp_multisite_cron'] as $option_name=>$option_value){
                        $this->update_option($option_name, $option_value);
                    }
                ?>
                    <div id="setting-error-settings_updated" class="updated settings-error">
                        <p>
                            <strong><?php _e('Settings saved.')?></strong>
                        </p>
                    </div>
                <?php else: ?>
                    <div id="message" class="error fade">
                        <p><?php _e('Unable to update settings.', self::$lang)?></p>
                    </div>
                <?php endif;?>
            <?php endif;?>



            <?php if(!is_multisite()):?>
                <div class="tool-box error">
                    <h3><?php _e('Multisite',self::$lang) ?></h3>
                    <p><?php _e('Your WordPress installation is not configured as multisite',self::$lang) ?>.</p>
                    <p><?php _e("Read the WordPress' documentation about multisite to learn how to set up multisite",self::$lang)?> : <a href="http://codex.wordpress.org/Create_A_Network" target="_blank"><?php _e('here',self::$lang)?></a></p>
                </div>
            <?php endif;?>


            <?php if(!defined('DISABLE_WP_CRON') || !DISABLE_WP_CRON ):?>
                <div class="tool-box error">
                    <h3><?php _e('WordPress default cron system',self::$lang) ?></h3>
                    <p><?php _e('You must disable the WordPress cron system, to use this extension',self::$lang) ?>.</p>
                    <p><?php _e('Add the following code in your wp-config.php',self::$lang) ?> :</p>
                    <p>
                        <code>define('DISABLE_WP_CRON', true);</code>
                    </p>
                </div>
            <?php endif;?>

            <form action="" method="post">
                <table class="form-table">
                    <tbody>
                        <tr valign="top">
                            <th scope="row">
                                <label for="concurrent_crons"><?php _e('Concurrent crons',self::$lang) ?></label><br />
                                <em><?php _e("Lower it if some of your sites' cron don't run",self::$lang)?></em>
                            </th>
                            <td>
                                <input type="text" name="wp_multisite_cron[concurrent_crons]" id="concurrent_crons" value="<?php echo $this->get_option('concurrent_crons') ?>" />
                            </td>
                        </tr>
                    </tbody>
                </table>
                <p class="submit">
                    <input class="button-primary" name="plugin_ok" value="<?php _e('Save settings',self::$lang) ?>" type="submit" />
                </p>
                <?php wp_nonce_field( plugin_basename( __FILE__ ), '_wpnonce' );?>
            </form>


            <?php if($this->is_ready()): ?>
                <div class="tool-box">
                    <h3><?php _e('Set-up WordPress multisite cron',self::$lang) ?></h3>
                    <p><?php _e('Add the following line in your crontab',self::$lang)?> :</p>
                    <p>
                        <code>*/15 * * * * www-data /usr/bin/wget -qO- <?php echo admin_url('admin-ajax.php','http').'?action=wp_multisite_cron_call' ?></code>
                    </p>
                </div>
            <?php endif;?>
        </div>

        <?php
    }

    /**
     *
     * Check wether the config is ok to run real crons
     */
    public function is_ready() {
        return (is_multisite() && defined('DISABLE_WP_CRON') && DISABLE_WP_CRON);
    }

    /**
     *
     * The real cron
     */
    public function cron_call() {
        $blogs = $this->get_blogs_id();

        if (!empty($blogs)) {
            $mh = curl_multi_init();

            $length = $this->get_option('concurrent_crons');
            $offset = 0;

            error_log('Initializing per-site cron tasks...');
            // Run concurrent crons
            while ($blogs_slice = array_slice($blogs,$offset,$length)) {
                $offset += $length;

                foreach ($blogs_slice as &$blog_id) {
                    switch_to_blog($blog_id);
                    $cron_url = site_url().'/wp-cron.php';
                    error_log('Queued '.$cron_url);

                    // Add the url to the stack
                    $chs[$blog_id] = curl_init();
                    curl_setopt($chs[$blog_id], CURLOPT_URL, $cron_url);
                    curl_setopt($chs[$blog_id], CURLOPT_HEADER, 0);
                    curl_multi_add_handle($mh, $chs[$blog_id]);
                }

                //Launch :)
                $running = null;
                do {
                    curl_multi_exec($mh, $still_running);
                } while($still_running > 0);

                curl_multi_close($mh);
            }
            error_log('Done.');
        }

        die();
    }

    /**
     *
     * Get the blog_id for each active blog in the multisite
     * @return array
     */
    private function get_blogs_id() {
        global $wpdb;

        //Get the blogs' ids for blogs that are public and active
        return $wpdb->get_col("SELECT blog_id FROM ".$wpdb->blogs." WHERE public = '1' AND archived = '0' AND spam = '0' AND deleted = '0';");
    }

    /**
     *
     * Get a plugin's specific option
     * @param string $option_name
     */
    public function get_option($option_name) {
        return get_option('wp_multisite_cron_'.$option_name);
    }

    /**
     *
     * Set a plugin's specific option
     * @param unknown_type $option_name
     */
    public function update_option($option_name,$option_value) {
        return update_option('wp_multisite_cron_'.$option_name,$option_value);
    }

    /**
     *
     * Delete a plugin's specific option
     * @param string $option_name
     */
    public function delete_option($option_name) {
        return delete_option('wp_multisite_cron_'.$option_name);
    }


    /**
     *
     * Get the plugin's path url
     */
    public function get_plugin_url() {
        return get_bloginfo('url') . '/' . PLUGINDIR . '/' . dirname(plugin_basename(__FILE__));
    }

    public static function init() {
        if (is_null(self::$instance)) {
            $instance = new WP_MultisiteCron();
        }

        return $instance;
    }
}

add_action('init', array('WP_MultisiteCron', 'init'));
